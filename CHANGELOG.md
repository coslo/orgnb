# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.1 - 2023/04/15

[Full diff](https://framagit.org/coslo/orgnb/-/compare/0.1.0...0.1.1)

###  Bug fixes
- Fix verbatim code blocks rendering

