orgnb
==================

Convert org-mode files to jupyter notebooks.

I could not get pandoc to convert org mode files properly, so I wrote this.

Quick start
-----------

```sh
orgnb test.org
```

Installation
------------
From pypi
```
pip install orgnb
```

From source
```
git clone git@framagit.org:coslo/orgnb.git
cd orgnb
make install
```

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/
